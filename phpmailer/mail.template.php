<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html charset=utf-8" />
<title>Message</title>
<style>
body
{
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	margin:0;
	padding:0;
	
	color:#666666;
}
table, tr, td{ border-collapse:collapse; vertical-align:top; }
td{padding:4px 10px;}
</style>
</head>

<body>
<div style="width:620px; background:#ffffff; margin:auto;">
<table>
  <tr>
    <td colspan="2"><br /><hr /><br /></td>
  </tr>
  <tr>
    <td colspan="2"><h3 style="font-family:"Century Gothic", Arial;">{key1} {val1}</h3></td>
  </tr>
  <tr>
    <td colspan="2"><br /><hr /><br /></td>
  </tr>

  <tr> 
    <td>{key2}</td>
    <td><strong>{val2}</strong></td>
  </tr>
  <tr>
    <td>{key3}</td>
    <td><strong>{val3}</strong></td>
  </tr>
  <tr>
  	<td>{key4}</td>
    <td><strong>{val4}</strong></td>
  </tr>
  <tr>
  	<td>{key5}</td>
    <td><strong>{val5}</strong></td>
  </tr>
	<tr>
  	<td>{key6}</td>
    <td><strong>{val6}</strong></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:50px 0; text-align:center; font-size:11px; color:#999; font-style:italic;"><strong>{thedate}</strong></td>
  </tr>
</table>
</div>
</body>
</html>