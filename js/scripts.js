// JavaScript Document

$(document).ready(function(){
	"use strict";
			
	$('#menu-icon').on('click', function(){
		
		$('ul.menu').slideToggle( "slow", function() {		
		
			if ($(this).is(':hidden')) {
				$('#menu-icon').html('&#9776;');
			} else {
				$('#menu-icon').html('&Chi;');
			}
		});
		
	});
	
	
	 if ( window.location.href === "http://stationnature.com/demande-informations.php" ||  window.location.href === "http://stationnature.com/demande-informations.php#top" ||  window.location.href === "http://stationnature.com/en/information-request.php" ||  window.location.href === "http://stationnature.com/en/information-request.php#top" ){
		// form validation
		$('#form_info, #form_info_en').validationEngine({promptPosition : "bottomRight"});		
		
		
			$('#entendu_parler').change( function(){
				
				var valeur = $(this).val();
				
				if( valeur === "Other" || valeur === "Autre" ){
					
					$('.precisez').show();
					
				}else{
					$('.precisez').hide();
				}
				
			});
		
	 }
	 
	 
	 
	 		var temp = $('#contact.row7 .inner .col2').height() - $('#contact.row7 .inner .col1').height();
			var newHeight = $('#map').height() - temp;
			$('#map').css({'height':newHeight});

		$(window).resize( function() {
			var temp = $('#contact.row7 .inner .col2').height() - $('#contact.row7 .inner .col1').height();
			var newHeight = $('#map').height() - temp;
			$('#map').css({'height':newHeight});
		});
	
		
	
	 if ( window.location.href != "http://stationnature.com/demande-informations.php" ||  window.location.href != "http://stationnature.com/information-request.php" || window.location.href != "http://stationnature.com/demande-informations.php#top" ||  window.location.href != "http://stationnature.com/information-request.php#top"){

	//alert('123');
		  // Add smooth scrolling to all links
		  $("body.accueil #menu-principal ul li a, footer a.top, .link").on('click', function(event) {
			  
			
			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
			  
			  // Prevent default anchor click behavior
			  event.preventDefault();
		
			  // Store hash
			  var hash = this.hash;
		
			  // Using jQuery's animate() method to add smooth page scroll
			  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			  $('html, body').animate({
				scrollTop: $(hash).offset().top
			  }, 800, function(){
		   
					// Add hash (#) to URL when done scrolling (default click behavior)
					window.location.hash = hash;
			  });
			  
			} // End if
			
		  });
  
  
 $("#menu-principal ul li a").on('click', function() {
			  $('ul.menu').slideToggle( "slow", function() {		
			
				if ($(this).is(':hidden')) {
					$('#menu-icon').html('&#9776;');
				} else {
					$('#menu-icon').html('&Chi;');
				}
			});
		});	
			
  
		$( '#carrousel' ).sliderPro({
			/*width: 375,*/
			height: 395,
			arrows:true,
			buttons:false,
			visibleSize: '100%',
			forceSize: 'fullWidth',
			autoSlideSize: false
		});

		// instantiate fancybox when a link is clicked
		$( '#carrousel .sp-image' ).parent( 'a' ).on( 'click', function( event ) {
			event.preventDefault();

			// check if the clicked link is also used in swiping the slider
			// by checking if the link has the 'sp-swiping' class attached.
			// if the slider is not being swiped, open the lightbox programmatically,
			// at the correct index
			if ( $( '#carrousel' ).hasClass( 'sp-swiping' ) === false ) {
				$.fancybox.open( $( '#carrousel .sp-image' ).parent( 'a' ), { index: $( this ).parents( '.sp-slide' ).index() } );
			}
		});


 } // if!= page demande information
 
 
 
 
 
 
 
 
 
 		$("#form_info").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() === "" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Veuillez remplir tous les champs requis.</div>");
				return false;
			}
	
		
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							//alert('OUI');
							
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event' : 'formSubmissionSuccess',
							'formId' : 'form_info'});


							$('#msg').html("<div class='msg-merci'><img src='//rtb.adgrx.com/segments/BRbOP3tuKf-m8mK2XBzoBu2ezN3EW-Ey4W_IDql3RKs=/30207.gif' width='1' height='1' border='0' />Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /></div>");
							$('#form_info').hide();
							if( window.innerWidth >= 767 ){
								$('#msg').css({'margin':'200px 0 500px 0'});
								 $('html, body').animate({ scrollTop: $('#top').offset().top}, 800);
							}
							$('.txt').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='erreur-msg'>Une erreur s'est produite, veuillez réessayer plus tard.<br /></br /><br /></div>");
							$('#form_info').hide();
							if( window.innerWidth >= 767 ){
								$('#msg').css({'margin':'200px 0 500px 0'});
								$('html, body').animate({ scrollTop: $('#top').offset().top}, 800);
}
							$('.txt').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='erreur-msg'>Veuillez remplir tous les champs requis.<br /><br /><br /></div>");
						}
						
							
							
						return false;
					}
				});
			
				return false;
	});
	
	
	
 		$("#form_info_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() === "" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='erreur-msg'>Please fill all the required fields</div>");
				return false;
			}
	
	
		
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							//alert('OUI');
							
							window.dataLayer = window.dataLayer || [];
							window.dataLayer.push({
							'event' : 'formSubmissionSuccess',
							'formId' : 'form_info_en'});



							$('#msg').html("<div class='msg-merci'><img src='//rtb.adgrx.com/segments/BRbOP3tuKf-m8mK2XBzoBu2ezN3EW-Ey4W_IDql3RKs=/30207.gif' width='1' height='1' border='0' />Thank you ! We have received your request. <br /> We will contact you shortly.<br /><br /><br /><br /><br /><br /></div>");
							$('#form_info_en').hide();
							if( window.innerWidth >= 767 ){
								$('#msg').css({'margin':'200px 0 500px 0'});
								$('html, body').animate({ scrollTop: $('#top').offset().top}, 800);
							}
							$('.txt').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='erreur-msg'>An error has occured, please try again later.</br /><br /><br /></div>");
							$('#form_info_en').hide();
							if( window.innerWidth >= 767 ){
								$('#msg').css({'margin':'200px 0 500px 0'});
								$('html, body').animate({ scrollTop: $('#top').offset().top}, 800);
							}
							$('.txt').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='erreur-msg'>Please fill all required fields.<br /><br /><br /></div>");
						}
						
							
							
						return false;
					}
				});
			
				return false;
	});
	
	
	
	

  
  
});