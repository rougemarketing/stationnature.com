<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<title>Station Nature | Condominiums Mont-Tremblant</title>
<meta name="description" content="Station Nature Condominiums: contemporary living spaces, two to three bedrooms, best price-quality ratio, St-Jovite sector of Mont-Tremblant.">

<meta property="og:image" content="http://stationnature.com/images/D-8586_500x395-A.jpg" />

<script src="../js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="../js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine-fr.js"></script>

<link rel="stylesheet" type="text/css" href="../css/slider-pro.min.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../css/validationEngine.jquery.css" media="screen"/>


<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style.min.css" rel="stylesheet" type="text/css" />

<script src="../js/scripts.js"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9JXKXG');</script>
<!-- End Google Tag Manager -->
</head>

<body class="accueil">

<?php include('../includes/header-en.inc.php'); ?>

<section id="img">
	<div class="inner">
    		
            <div class="img-title">
                <h1>My Affordable<br />
					Mont-Tremblant Condo</h1>
                <h2>Condos with 2 to 3 bedrooms</h2>
            </div><!-- img-title -->
        
    </div><!-- inner -->
</section><!-- img -->



<section id="project" class="row1">
	<div class="inner">
        
            <div class="col1">
            	<div class="img-famille"></div><!-- img-famille -->
            </div><!-- col1 -->
            
            <div class="col2">
            	<div class="content">
                		
                       <h3>Station Your Life in Nature</h3>

                       <h4>Your very own slice of earthly paradise<br />
							from only $149,900</h4>

                        
                       <p>Do you dream of living, investing or owning a pied à terre in the stunning Mont-Tremblant region? Station Nature offers contemporary living spaces, smartly designed, with two to three bedrooms and liveable floor space ranging from 1,239 to 1,279 square feet, at the best price-quality ratio in the region. </p>

						<p>Each unit features a terrace or balcony as well as lots of fenestration and two parking spaces. Modern, bright, and spacious, our condominiums offer spectacular views of the surrounding nature and mountains, mere steps from the P’tit Train du Nord linear park. </p> 

						<a href="#plans" class="link btn-vert-2">View Floorplans</a>

                        
                
                </div><!-- content -->
            </div><!-- col2 -->
        
    </div><!-- inner -->
</section><!-- projet / row1 -->


<section id="row2">
	<div class="inner">
		
	<div id="carrousel" class="slider-pro">
		<div class="sp-slides">
        
        
        
        
        
			<div class="sp-slide">
				<a href="../images/D-8586_Carrousel-2-big.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_Carrousel-2.jpg" 
						data-retina="../images/D-8586_Carrousel-2.jpg"/>
				</a>
			</div>
            
            
        
			<div class="sp-slide">
				<a href="../images/D-8842_Images_500x395-A.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8842_Images_500x395-A.jpg" 
						data-retina="../images/D-8842_Images_500x395-A.jpg"/>
				</a>
			</div>
        
        
			<div class="sp-slide">
				<a href="../images/D-8842_Images_500x395-B.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8842_Images_500x395-B.jpg" 
						data-retina="../images/D-8842_Images_500x395-B.jpg"/>
				</a>
			</div>
            
            
        
			<div class="sp-slide">
				<a href="../images/D-8586_Carrousel-9-big.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_Carrousel-9.jpg" 
						data-retina="../images/D-8586_Carrousel-9.jpg"/>
				</a>
			</div>
        
        
			<div class="sp-slide">
				<a href="../images/D-8586_Carrousel-5-big.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_Carrousel-5.jpg" 
						data-retina="../images/D-8586_Carrousel-5.jpg"/>
				</a>
			</div>
        
        
			<div class="sp-slide">
				<a href="../images/D-8586_Carrousel-1-big.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_Carrousel-1.jpg" 
						data-retina="../images/D-8586_Carrousel-1.jpg"/>
				</a>
			</div>
        
			<div class="sp-slide">
				<a href="../images/D-8586_Carrousel-8-big.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_Carrousel-8.jpg" 
						data-retina="../images/D-8586_Carrousel-8.jpg"/>
				</a>
			</div>
        
        
			<div class="sp-slide">
				<a href="../images/station-nature-01.jpg">
					<img class="sp-image" src="../css/images/blank.gif" 
						data-src="../images/D-8586_500x395-A.jpg" 
						data-retina="../images/D-8586_500x395-A.jpg"/>
				</a>
			</div>



		</div>
    </div>
        
        
        
        
        
        
        
        
        
	</div><!-- inner -->
</section><!-- row2 -->



<section id="site" class="row3">
	<div class="inner">
		
            <div class="col1">
                <div class="content">
                              
                    <h3>Peace and Quiet, 
                        Next to the Action</h3>
                    
                    <h4>Live in coveted Mont-Tremblant</h4>
                    
                    <p>Station Nature is located close to Domaine Saint-Bernard in downtown Mont-Tremblant, within walking distance from numerous restaurants, shops and boutiques. Just a short drive to the Mont-Tremblant resort village and #1-ranked ski resort in Eastern North America, as well as Mont Blanc, Station Nature gives you the best of Tremblant,  in a pristine woodland setting with unobstructed views thanks to buried cables. </p>
                    
                    <p>Outdoor activity aficionado? Lace up those hiking boots or break out the snow shoes, X-country skis, or mountain bike and explore the legendary multi-functional P’tit Train du Nord linear park!</p> 
                    
                    <a href="https://mont-tremblant.ca/en/" class="btn-vert-2" target="_blank">Discover Mont-Tremblant</a>

				</div><!-- content -->
            </div><!-- col1 -->
            
            <div class="col2">
               <div class="img-femme"></div><!-- img-femme -->
            </div><!-- col2 -->        
        
        
	</div><!-- inner -->
</section><!-- site / row3 -->



<section id="row4"></section><!-- row4 -->



<section id="plans" class="row5">
	<div class="inner">


	
    		<h3>Your Needs, Your Space</h3>

            <h4><a href="../pdf/plans-station-nature.pdf" target="_blank">Picture your floor plan</a></h4>
            
            <p>Station Nature is made up of 12 condominiums divided across three phases. Phase 1 is complete, while Phases 2 and 3 are currently on sale. Some units are ready for immediate occupancy.</p>
            
            <p>A variety of options are available for you to personalize your living space: electric fireplace, air conditioning, modern interior finishes, etc.</p>

			<a href="#contact" class="link btn-vert-2">Talk to a representative<br />to find out more </a> 
            
            
            <br><br><br>
            <br><br><br>

	</div><!-- inner -->
</section><!-- plans / row5 -->




<section class="row6"></section>


<section id="contact" class="row7">
	<div class="inner">

            <div class="col1">
            
            	<div class="img-ski"></div><!-- img-ski -->
            
            </div><!-- col1 -->
            
            <div class="col2">
            
            	<div class="content">

                        <h3>Habitations Luma</h3>
                        
                        <h4>Dream quality at down-to-earth prices</h4>
                        
                        <p>Come and visit our sales office or speak to a housing advisor for more information.</p>
                        
                        <p>Fernand Sabourin, Royal Lepage<br />
                        <a href="tel:8193414663" class="phone">819 341-4663</a></p>
                        
                        <p>Sales Office <br />	
                        1009-2, rue Labelle (Mont-Tremblant)</p>
                        
                        <a href="https://www.facebook.com/Habitations-LUMA-778891942131706" class="facebook" target="_blank"></a>

                        
                
                </div><!-- content -->
                
                <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2764.9916364188152!2d-74.59273628442007!3d46.13099647911446!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ccf75abb7801973%3A0x5f00003b926e263c!2s1009+Rue+Labelle%2C+Mont-Tremblant%2C+QC+J8E+2W5!5e0!3m2!1sen!2sca!4v1481293563878" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe><!-- map -->
		  
            
            </div><!-- col2 -->


	</div><!-- inner -->
</section><!-- contact -->

<div class="map-mobile"></div><!-- map-mobile -->


<?php include('../includes/footer-en.inc.php'); ?>

</body>
</html>