jQuery(function($) {
    "use strict";
	
	
		$('.close-popup').click( function(){ 
			$('#popup').hide();
		});
		



		/* page communauté à Gatineau */
		
		$('.boite-projet').on('click', function() {
			
			
			  var id = $(this).attr('id');
			  
			  if (id == "projet1") {
				  window.open("http://galerie151.com", "_blank");
			  }else if (id == "projet2") {
				  window.open("http://lecentralcondominiums.com/", "_blank");
			  }else if (id == "projet3") {
				 window.open("http://www.junic.ca/y/", "_blank");
			  }	else if (id == "projet1_en") {
				  window.open("http://galerie151.com/en/index.php", "_blank");
			  }else if (id == "projet2_en") {
				  window.open("http://lecentralcondominiums.com/en/", "_blank");
			  }else if (id == "projet3_en") {
				 window.open("http://www.junic.ca/y/en/", "_blank");
			  }
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});
	
	
	
	
	/* Cliquez au complet sur la vignette du projet de la page d'accueil */
	
		$('.click-projet1').on('click', function() {
			
				window.open("http://galerie151.com", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet2').on('click', function() {
			  
			 window.open("http://lecentralcondominiums.com/", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet3').on('click', function() {
			
				 window.open("http://www.junic.ca/y/", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet4').on('click', function() {
			
				 window.open("http://www.junic.ca/fusion/index.php", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});





	/* Cliquez au complet sur la vignette du projet de la page d'accueil ANGLAISE */
	
		$('.click-projet1-en').on('click', function() {
			
				window.open("http://galerie151.com/en/index.php", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet2-en').on('click', function() {
			  
			 window.open("http://lecentralcondominiums.com/en/", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet3-en').on('click', function() {
			
				 window.open("http://www.junic.ca/y/en/", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});


		$('.click-projet4-en').on('click', function() {
			
				 window.open("http://www.junic.ca/fusion/en/index.php", "_blank");
			  
		}).on('mouseover', function() {
			
			 $(this).css({'cursor': 'pointer'}); 
		});







	
	
		$('.hide-desktop').html("+")

		$('.hide-desktop').click(function(){
			
				$('.hide-mobile').slideToggle('fast', function(){
						if($('.hide-mobile').is(':hidden')){
							$('.hide-desktop').html("+");
						}else{
							$('.hide-desktop').html("-");
						}
				});
			
		});
	
		$('.hide-desktop2').html("+")

		$('.hide-desktop2').click(function(){
			
				$('.hide-mobile2').slideToggle('fast', function(){
						if($('.hide-mobile2').is(':hidden')){
							$('.hide-desktop2').html("+");
						}else{
							$('.hide-desktop2').html("-");
						}
				});
			
		});
			
	
		$('.hide-desktop3').html("+")

		$('.hide-desktop3').click(function(){
			
				$('.hide-mobile3').slideToggle('fast', function(){
						if($('.hide-mobile3').is(':hidden')){
							$('.hide-desktop3').html("+");
						}else{
							$('.hide-desktop3').html("-");
						}
				});
			
		});
			
	
		$('.hide-desktop4').html("+")

		$('.hide-desktop4').click(function(){
			
				$('.hide-mobile4').slideToggle('fast', function(){
						if($('.hide-mobile4').is(':hidden')){
							$('.hide-desktop4').html("+");
						}else{
							$('.hide-desktop4').html("-");
						}
				});
			
		});
		
	
		//$('.hide-desktop-btn').html("+")

		$('.hide-desktop-btn').click(function(){
			
				$('.hide-mobile-btn').slideToggle('fast', function(){
						if($('.hide-mobile-btn').is(':hidden')){
							$('.hide-desktop-btn').html("<h3 class='blanc'><strong>RÉSERVATION PRIORITAIRE +</strong></h3>");
						}else{
							$('.hide-desktop-btn').html("<h3 class='blanc'><strong>RÉSERVATION PRIORITAIRE -</strong></h3>");
						}
				});
			
		});

		$('.hide-desktop-btn2').click(function(){
			
				$('.hide-mobile-btn2').slideToggle('fast', function(){
						if($('.hide-mobile-btn2').is(':hidden')){
							$('.hide-desktop-btn2').html("<h3 class='blanc'><strong>ACHETER UNE PROPRIÉTÉ +</strong></h3>");
						}else{
							$('.hide-desktop-btn2').html("<h3 class='blanc'><strong>ACHETER UNE PROPRIÉTÉ -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn3').click(function(){
			
				$('.hide-mobile-btn3').slideToggle('fast', function(){
						if($('.hide-mobile-btn3').is(':hidden')){
							$('.hide-desktop-btn3').html("<h3 class='blanc'><strong>LOUER UNE PROPRIÉTÉ +</strong></h3>");
						}else{
							$('.hide-desktop-btn3').html("<h3 class='blanc'><strong>LOUER UNE PROPRIÉTÉ -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn4').click(function(){
			
				$('.hide-mobile-btn4').slideToggle('fast', function(){
						if($('.hide-mobile-btn4').is(':hidden')){
							$('.hide-desktop-btn4').html("<h3 class='blanc'><strong>LOUER UN ESPACE COMMERCIAL +</strong></h3>");
						}else{
							$('.hide-desktop-btn4').html("<h3 class='blanc'><strong>LOUER UN ESPACE COMMERCIAL -</strong></h3>");
						}
				});
			
		});


		$('.hide-desktop-btn5').click(function(){
			
				$('.hide-mobile-btn5').slideToggle('fast', function(){
						if($('.hide-mobile-btn5').is(':hidden')){
							$('.hide-desktop-btn5').html("<h3 class='blanc'><strong>CONTACTEZ NOTRE ÉQUIPE +</strong></h3>");
						}else{
							$('.hide-desktop-btn5').html("<h3 class='blanc'><strong>CONTACTEZ NOTRE ÉQUIPE -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn6').click(function(){
			
				$('.hide-mobile-btn6').slideToggle('fast', function(){
						if($('.hide-mobile-btn6').is(':hidden')){
							$('.hide-desktop-btn6').html("<h3 class='blanc'><strong>J'AIMERAIS INVESTIR +</strong></h3>");
						}else{
							$('.hide-desktop-btn6').html("<h3 class='blanc'><strong>J'AIMERAIS INVESTIR -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn7').click(function(){
			
				$('.hide-mobile-btn7').slideToggle('fast', function(){
						if($('.hide-mobile-btn7').is(':hidden')){
							$('.hide-desktop-btn7').html("<h3 class='blanc'><strong>DEMANDE D'INFORMATIONS +</strong></h3>");
						}else{
							$('.hide-desktop-btn7').html("<h3 class='blanc'><strong>DEMANDE D'INFORMATIONS -</strong></h3>");
						}
				});
			
		});
			


/* EN */


		$('.hide-desktop-btn-en').click(function(){
			
				$('.hide-mobile-btn-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn-en').is(':hidden')){
							$('.hide-desktop-btn-en').html("<h3 class='blanc'><strong>PRIORITY REGISTRATION +</strong></h3>");
						}else{
							$('.hide-desktop-btn-en').html("<h3 class='blanc'><strong>PRIORITY REGISTRATION -</strong></h3>");
						}
				});
			
		});

		$('.hide-desktop-btn2-en').click(function(){
			
				$('.hide-mobile-btn2-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn2-en').is(':hidden')){
							$('.hide-desktop-btn2-en').html("<h3 class='blanc'><strong>OWN A PROPERTY +</strong></h3>");
						}else{
							$('.hide-desktop-btn2-en').html("<h3 class='blanc'><strong>OWN A PROPERTY -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn3-en').click(function(){
			
				$('.hide-mobile-btn3-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn3-en').is(':hidden')){
							$('.hide-desktop-btn3-en').html("<h3 class='blanc'><strong>RENT A UNIT +</strong></h3>");
						}else{
							$('.hide-desktop-btn3-en').html("<h3 class='blanc'><strong>RENT A UNIT -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn4-en').click(function(){
			
				$('.hide-mobile-btn4-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn4-en').is(':hidden')){
							$('.hide-desktop-btn4-en').html("<h3 class='blanc'><strong>RENT SPACE +</strong></h3>");
						}else{
							$('.hide-desktop-btn4-en').html("<h3 class='blanc'><strong>RENT SPACE -</strong></h3>");
						}
				});
			
		});


		$('.hide-desktop-btn5-en').click(function(){
			
				$('.hide-mobile-btn5-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn5-en').is(':hidden')){
							$('.hide-desktop-btn5-en').html("<h3 class='blanc'><strong>CONTACT OUR TEAM +</strong></h3>");
						}else{
							$('.hide-desktop-btn5-en').html("<h3 class='blanc'><strong>CONTACT OUR TEAM -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn6-en').click(function(){
			
				$('.hide-mobile-btn6-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn6-en').is(':hidden')){
							$('.hide-desktop-btn6-en').html("<h3 class='blanc'><strong>I WANT TO INVEST +</strong></h3>");
						}else{
							$('.hide-desktop-btn6-en').html("<h3 class='blanc'><strong>I WANT TO INVEST -</strong></h3>");
						}
				});
			
		});
			


		$('.hide-desktop-btn7-en').click(function(){
			
				$('.hide-mobile-btn7-en').slideToggle('fast', function(){
						if($('.hide-mobile-btn7-en').is(':hidden')){
							$('.hide-desktop-btn7-en').html("<h3 class='blanc'><strong>INFORMATION REQUEST +</strong></h3>");
						}else{
							$('.hide-desktop-btn7-en').html("<h3 class='blanc'><strong>INFORMATION REQUEST -</strong></h3>");
						}
				});
			
		});





		// auto validation of the forms
		$('form#formulaire_contact_fr, form#formulaire_reservation_prioritaire_fr, form#formulaire_achat_fr, form#form_location_residentielle_fr, form#form_location_commerciale_fr, form#form_contact_autre_fr, form#formulaire_footer_contact_fr, form#formulaire_sondage_fr').validationEngine();		
		
		$('form#formulaire_contact_en, form#formulaire_reservation_prioritaire_en, form#formulaire_achat_en, form#form_location_residentielle_en, form#form_location_commerciale_en, form#form_contact_autre_en, form#formulaire_footer_contact_en, form#formulaire_sondage_en').validationEngine();		

		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE DE CONTACT
	/** FR
	/**
	/**************************************/	
	
		
		$("#formulaire_contact_fr").bind("submit", function() {

			var erreur = '';
			
			if ( $("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							$('#formulaire_contact_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.<br /><br /></div>");
							$('#formulaire_contact_fr').hide();
						}
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
						return false;
					}
				});
			
				return false;
	});

		
		
		
		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE DE RÉSERVATION PRIORITAIRE
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#formulaire_reservation_prioritaire_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_reservation_prioritaire_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#formulaire_reservation_prioritaire_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
	
	
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE D'ACHAT
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#formulaire_achat_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_achat_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#formulaire_achat_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
		
		
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE LOCATION RESIDENTIELLE
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#form_location_residentielle_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_location_residentielle_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#form_location_residentielle_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE LOCATION COMMERCIALE
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#form_location_commerciale_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "autre";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_location_commerciale_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#form_location_commerciale_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE CONTACT AUTRE
	/** PAGE GESTION DE PROPRIÉTÉ OU REVENTE
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#form_contact_autre_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "autre";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_contact_autre_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#form_contact_autre_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
 
 
 
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE CONTACT FOOTER
	/** FOOTER
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#formulaire_footer_contact_fr").bind("submit", function() {

			var erreur = '';
				
			if ($("#nom2").val().length < 1 || $("#prenom2").val().length < 1 || $("#courriel2").val().length < 1 || $("#code_postal2").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg2').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						//console.log(data);
						if(data == 1){
							if($("#infolettre2").is(':checked')){
								//alert('OUI');
								var form_footer = "oui";
								//sendAdnetisDB(form_footer);
								$('#msg2').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> Vous recevrez un courriel pour confirmer votre abonnement à notre liste de diffusion.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg2').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_footer_contact_fr').hide();
						}
						
						else if(data == 2){
							$('#msg2').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div>");
							$('#formulaire_footer_contact_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg2').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 /*******************************************/
 /*              FORMUALIRE EN              */
 /*******************************************/
 
 
 
 
 
		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE DE CONTACT
	/** EN
	/**
	/**************************************/	
	
		
		$("#formulaire_contact_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								//sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							$('#formulaire_contact_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.<br /><br /></div>");
							$('#formulaire_contact_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});

		
		
		
		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE DE RÉSERVATION PRIORITAIRE
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#formulaire_reservation_prioritaire_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_reservation_prioritaire_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#formulaire_reservation_prioritaire_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
	
	
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE D'ACHAT
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#formulaire_achat_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_achat_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#formulaire_achat_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
		
		
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE LOCATION RESIDENTIELLE
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#form_location_residentielle_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "non";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_location_residentielle_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#form_location_residentielle_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
	
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE LOCATION COMMERCIALE
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#form_location_commerciale_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "autre";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_location_commerciale_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#form_location_commerciale_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE CONTACT AUTRE
	/** PAGE GESTION DE PROPRIÉTÉ OU REVENTE
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#form_contact_autre_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#entendu_parler").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								var form_footer = "autre";
								sendAdnetisDB(form_footer);
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#form_contact_autre_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#form_contact_autre_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
 
 
 
		
		
		
	/**************************************/	
	/**
	/**	FORMULAIRE CONTACT FOOTER
	/** FOOTER
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#formulaire_footer_contact_en").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom2").val().length < 1 || $("#prenom2").val().length < 1 || $("#courriel2").val().length < 1 || $("#code_postal2").val().length < 1 || $("#entendu_parler2").val() == "choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg2').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
				return false;
			}
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
						if(data == 1){
							if($("#infolettre2").is(':checked')){
								//alert('OUI');
								var form_footer = "oui";
								sendAdnetisDB(form_footer);
								$('#msg2').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> You will receive an email to confirm your subscription to our mailing list.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}else{
								$('#msg2').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div>");
							}
							
							$('#formulaire_footer_contact_en').hide();
						}
						
						else if(data == 2){
							$('#msg2').html("<div class='ale-alert red'><br />An error has occurred, please try again later.</br /><br /></div>");
							$('#formulaire_footer_contact_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg2').html("<div class='ale-alert red'><br />Please fill in all required fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
 
 
 
 







		
	/**************************************/	
	/**
	/**	FORMULAIRE SONDAGE CARTE DE VISITE
	/** FR
	/**
	/**************************************/	
	
	
	
		$("#formulaire_sondage_fr").bind("submit", function() {

			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#telephone").val().length < 1 || $('#age').val()== "Choisir"  || $('#prevoit_emmenager').val()== "Choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
				return false;
			}
		
		
			/**if ($("#je_suis_interesse:checked").length < 1){
				//alert('erreur checkbox');
				erreur = 2;
				$('#msg').html("<div class='msg-erreur'>Oups!<br />Veuillez présicer par quel projet vous êtes intéressé.</div>");
				return false;
			}*/
	
	
			
				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
					//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /> <br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div><br /><br /><a href='/carte-de-visite-sondage-visiteur/' class='btn-retour' style='color:#fff;'>Retour au sondage</a>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Merci! Nous avons bien reçu votre demande.<br /> Nous vous contacterons sous peu.<br /><br /><strong>SUIVEZ-NOUS :</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div><br /><br /><a href='/carte-de-visite-sondage-visiteur/' class='btn-retour'  style='color:#fff;'>Retour au sondage</a>");
							}
							
							$('#formulaire_sondage_fr').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />Une erreur s'est produite, veuillez réessayer plus tard.</br /><br /></div><br /><br /><a href='/carte-de-visite-sondage-visiteur/' class='btn-retour'  style='color:#fff;'>Retour au sondage</a>");
							$('#formulaire_sondage_fr').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />Veuillez remplir tous les champs requis.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		



		
	/**************************************/	
	/**
	/**	FORMULAIRE SONDAGE CARTE DE VISITE
	/** EN
	/**
	/**************************************/	
	
	
	
		$("#formulaire_sondage_en").bind("submit", function() {

		
	
			var erreur = '';
			
			if ($("#nom").val().length < 1 || $("#prenom").val().length < 1 || $("#courriel").val().length < 1 || $("#code_postal").val().length < 1 || $("#telephone").val().length < 1 || $('#age').val()== "Choisir"  || $('#prevoit_emmenager').val()== "Choisir" ) {
				/**$("#error").show();
				$.fancybox.resize();*/
				//alert('erreur');
				erreur = 1;
				$('#msg').html("<div class='ale-alert red'><br />You must fill all the fields.<br /><br /></div>");
				return false;
			}
 
 
 
 				$.ajax({
					type		: "POST",
					cache	: false,
					url		: "/wp-content/themes/gimnasio-child/post-formulaire.php",
					data		: $(this).serializeArray(),
					success: function(data) {
						//$.fancybox(data);
						//alert(data);
					//console.log(data);
						if(data == 1){
							if($("#infolettre").is(':checked')){
								//alert('OUI');
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /> <br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div><br /><br /><a href='/en/customer-survey/' class='btn-retour' style='color:#fff;'>Back to the survey</a>");
							}else{
								$('#msg').html("<div class='ale-alert grey'>Thank you ! We have received your request.<br /> We will contact you shortly.<br /><br /><strong>FOLLOW US:</strong><div><p class='icons_item_background'><a class='facebook' href='https://www.facebook.com/ConstructionJunic' target='_blank'></a></p><p class='icons_item_background'><a class='instagram' href='http://instagram.com/constructionjunic/' target='_blank'></a></p></div></div><br /><br /><a href='/en/customer-survey/' class='btn-retour' style='color:#fff;'>Back to the survey</a>");
							}
							
							$('#formulaire_sondage_en').hide();
						}
						
						else if(data == 2){
							$('#msg').html("<div class='ale-alert red'><br />An error has occured, please try again later.</br /><br /></div><br /><br /><a href='/en/customer-survey/' class='btn-retour' style='color:#fff;'>Back to the survey</a>");
							$('#formulaire_sondage_en').hide();
						}
						
						
						else if(data == 3){
							$('#msg').html("<div class='ale-alert red'><br />You musy fill all the fields.<br /><br /></div>");
							//$('#formulaire_reservation_prioritaire_fr').hide();
						}
							
							
						return false;
					}
				});
			
				return false;
	});
		
		
		
		
		
 		$("input[type='radio']").click(function(){
            var radioValue = $("input[name='locataire_proprio']:checked").val();
            if(radioValue == "Propriétaire"){
				
             	 $('.vendre_maison').show();
            }else{
				 $('.vendre_maison').hide();
				}
        });
		


  		/*$("input[type='checkbox']").click(function(){
			
			
           
            if( $(this).attr("value") == "Déjà client"){
				
             	 $('.deja_client').show();
            }
			
			
			
			
			
        });*/


 

/* $('#provenance_publicitaire').change(
    function() {
      if( $(this).val() == "Déjà client (précision, raison du retour)" ){
		  $('.deja_client').show();
	   }else{
		   $('.deja_client').hide();
	   }
	   
      if( $(this).val() == "Référé par" ){
		  $('.refere_par').show();
	   }else{
		   $('.refere_par').hide();
	   }	  
	   
	   
      if( $(this).val() == "Autre" ){
		  $('.autre').show();
	   }else{
		   $('.autre').hide();
	   }	   
	   
    }
	
	
	
);*/

 
 
}); // jQuery(document).ready(function($)








var xmlhttp;
function loadXMLDoc(url,cfunc)
{
                if (window.XMLHttpRequest)
                  {// code for IE7+, Firefox, Chrome, Opera, Safari
                  xmlhttp=new XMLHttpRequest();
                  }
                else
                  {// code for IE6, IE5
                  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                  }
                xmlhttp.onreadystatechange=cfunc;
                xmlhttp.open("GET",url,true);
                xmlhttp.send();
}


function sendAdnetisDB(form_footer)
{
	
	if( form_footer == "non" ){
		
				var strnom = document.getElementById("nom").value;
				var strprenom = document.getElementById("prenom").value;
                var strname = strprenom + ' ' + strnom; /*document.getElementById("subscribeName").value;*/
                var stremail = document.getElementById("courriel").value;
				
				var strcodepostal = document.getElementById("code_postal").value;
                var strtelephone = document.getElementById("telephone").value;

				
				var options_choisis = []; 
				var cboxes = document.getElementsByName('je_suis_interesse[]');
				
				
				var len = cboxes.length;
					
					for (var i=0; i<len; i++) {
						if(cboxes[i].type == 'checkbox' && cboxes[i].checked == true){
							 options_choisis.push(cboxes[i].value);
							
						}
						//alert(i + (cboxes[i].checked?' checked ':' unchecked ') + cboxes[i].value);
					}
				
	}else if( form_footer == "oui"  ){
		
				var strnom = document.getElementById("nom2").value;
				var strprenom = document.getElementById("prenom2").value;
                var strname = strprenom + ' ' + strnom; /*document.getElementById("subscribeName").value;*/
                var stremail = document.getElementById("courriel2").value;
				
				var strcodepostal = document.getElementById("code_postal2").value;
                var strtelephone = document.getElementById("telephone2").value;

				
				var options_choisis = []; 
				var cboxes = document.getElementsByName('je_suis_interesse2[]');
				
				
				var len = cboxes.length;
					
					for (var i=0; i<len; i++) {
						if(cboxes[i].type == 'checkbox' && cboxes[i].checked == true){
							 options_choisis.push(cboxes[i].value);
							
						}
						//alert(i + (cboxes[i].checked?' checked ':' unchecked ') + cboxes[i].value);
					}
				
	}else if( form_footer == "autre"  ){ /* por formulaire Location Commerciale / Gestion de propriété / Revente */
		
				var strnom = document.getElementById("nom").value;
				var strprenom = document.getElementById("prenom").value;
                var strname = strprenom + ' ' + strnom; /*document.getElementById("subscribeName").value;*/
                var stremail = document.getElementById("courriel").value;
				
				var strcodepostal = document.getElementById("code_postal").value;
                var strtelephone = document.getElementById("telephone").value;
                var options_choisis = document.getElementById("je_suis_interesse").value;
	}
			
				
				var id_adnetis = "";
				
				if( document.getElementById("langue").value  == "fr" ){
					
					id_adnetis = "naXfnat2AYMEUsRw0D1LAjDmBk7Cgk24";
					
				}else{
					
					id_adnetis = "naXfnat2AYMEkK80G9nE2EE_2fjpfHQbS5";
					
				}
				
						var path = "http://solutions-emailing.com/scripts/scripts.aspx?Type=subscribe&subscribeHidden=1&Perso=''"
						path += "&subscribeName=" + strname
						path += "&subscribeEmail=" + stremail
						path += "&subscribeCP=" + strcodepostal
						path += "&subscribeTel=" + strtelephone
						path += "&subscribesecteur=" + options_choisis
						path += "&Id=" + id_adnetis
						
				
				alert (path);
				
				
            
                loadXMLDoc(path,function()
                  {
                  if (xmlhttp.readyState==4 && xmlhttp.status==200)
                               {
                               document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
                               }
                  });
}





/*

// Returns an array with values of the selected (checked) checkboxes in "frm"
function getSelectedChbox(frm) {
 // JavaScript & jQuery Course - http://coursesweb.net/javascript/
  var selchbox = [];        // array that will store the value of selected checkboxes

  // gets all the input tags in frm, and their number
  var inpfields = frm.getElementsByTagName('input');
  var nr_inpfields = inpfields.length;

  // traverse the inpfields elements, and adds the value of selected (checked) checkbox in selchbox
  for(var i=0; i<nr_inpfields; i++) {
    if(inpfields[i].type == 'checkbox' && inpfields[i].checked == true) selchbox.push(inpfields[i].value);
  }

  return selchbox;
}

  /* Test this function */
// When click on #btntest, alert the selected values
/*document.getElementById('btntest').onclick = function(){
  var selchb = getSelectedChbox(this.form);     // gets the array returned by getSelectedChbox()
  alert(selchb);
}				
	*/			

