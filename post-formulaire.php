<?php
$form_action = '';
date_default_timezone_set('America/New_York');

	
	/*define('APPLICATION_ROOT',	'/home/devrouge/subdomains/stationnature/dev/');
	define('APPLICATION_URL', 	'http://stationnature.dev.devrouge.com');*/
	define('APPLICATION_ROOT',	'/home/stationnature/public_html/');
	define('APPLICATION_URL', 	'http://stationnature.com');
	
	   
	
	define('ADMIN_ROUGE',		'julie@rougemarketing.com');
	define('ROUGE',				'line@rougemarketing.com');
	define('CLIENT1',			'fernandsabourin@royallepage.ca');
	define('CLIENT2',			'cgrenon.sutton@gmail.com');


	define('CLIENT',		'info@stationnature.com');


require APPLICATION_ROOT . 'phpmailer/classes/PHPMailer_5.2.0/class.phpmailer.php';



$mail	= new PHPMailer(); // defaults to using php "mail()"	

$output	= file_get_contents(APPLICATION_ROOT . 'phpmailer/mail.template.php');



switch($_POST['action'])

{

	case 'info' :	
		
		$langue_fr = '';
		$langue_en = '';
		$listId = '36b83209e2'; // MailChimp // ID liste // infolettre station nature
		$subjectmail = "Demande d'informations | Station Nature";
		$infolettre = '';
	
		if( $_POST['entendu_parler'] == "" || $_POST['entendu_parler'] == "Choisir" || $_POST['nom'] == "" || $_POST['prenom'] == "" || $_POST['courriel'] == "" || $_POST['code_postal'] == "" ){
			$form_action = 3;
			echo $form_action;
			exit();
		}
		
		
		
		if (!filter_var($_POST['courriel'], FILTER_VALIDATE_EMAIL)) {
			$form_action = 3;
			echo $form_action;
			exit();
		}
		
		if( $_POST['subject'] != "" ){
			$form_action = 3;
			echo $form_action;
			exit();
		}
		
		
		if( $_POST['langue'] == "fr" ){
			$langue_fr = true;
			$langue_en = false;
		}else if( $_POST['langue'] == "en"  ){
			$langue_fr = false;
			$langue_en = true;
		}else{  // FR par défaut
			$langue_fr = true;
			$langue_en = false;
		}
			

				
/*****************************************************	

	MAILCHIMP DATA 

*****************************************************/		
		
		
	if( $_POST['infolettre'] == "oui" ){
			
		$infolettre = 'oui';

		
		
		$data       = array(
                    'apikey'        => $apikey,
                    'email_address' => htmlentities($_POST['courriel']),
                    'status'        => 'subscribed',  /* écrire subscribed si on ne veut pas de courriel pour confirmer l'adresse courriel, écrire pending pour que la personne confirme son adresse */
                    'merge_fields'  => array(
											'FNAME' => $_POST['prenom'],
											'LNAME' => $_POST['nom'],
											'EMAIL' => $_POST['courriel'],
											'MMERGE5' => $_POST['telephone'],	/*	telephone	*/
											'MMERGE6' => $_POST['code_postal'],		/*	code postal	*/
											'MMERGE3' => $_POST['entendu_parler'],		/* Entendu parler  */
											'MMERGE4' => $_POST['autre_precisez'],		/* Entendu parler - autre précision */
											'MMERGE7' => $_POST['commentaires'],		/* Commentaires  */
										
									   ), 

									   	'interests'     => array( 
																'0693418652' => $langue_fr,   /* FR */
																'b958d41cb7' => $langue_en,   /* EN */
														),															
																 
									    'update_existing'   => true);	
				
				
			
				sendDataToMailChimp($listId, $_POST['courriel'], $data);
					
				
				
		}else{
					$infolettre = 'non';

			
		}/* END subscribed to infolettre */
			
/*****************************************************	

	END MAILCHIMP DATA 

*****************************************************/		



					
					
		$output	= file_get_contents(APPLICATION_ROOT . 'phpmailer/mail.template.php');

		$body = preg_replace('/{siteurl}/'	, APPLICATION_URL, $output);
		
		$body = preg_replace('/{key1}/'	, 'Station Nature | Formulaire',$body); 

		$body = preg_replace('/{val1}/'	, '', $body);

		$body = preg_replace('/{key2}/'	, 'De', $body);

		$body = preg_replace('/{val2}/'	, $_POST['prenom'] . ' ' . $_POST['nom'] , $body);	

		$body = preg_replace('/{key3}/'	, 'Courriel', $body);

		$body = preg_replace('/{val3}/'	, $_POST['courriel'] , $body);	

		$body = preg_replace('/{key4}/'	, 'Code postal', $body);

		$body = preg_replace('/{val4}/'	, $_POST['code_postal'] , $body);	

		$body = preg_replace('/{key5}/'	, 'Téléphone', $body);

		$body = preg_replace('/{val5}/'	, $_POST['telephone'] , $body);

		$body = preg_replace('/{key6}/'	, 'Comment avez-vous entendu parler?', $body);

		$body = preg_replace('/{val6}/'	, $_POST['entendu_parler'] , $body);

		$body = preg_replace('/{key7}/'	, 'Commentaires', $body);

		$body = preg_replace('/{val7}/'	, $_POST['commentaires'] , $body);
		
		$body = preg_replace('/{key8}/'	, 'Infolettre', $body);
		
		$body = preg_replace('/{val8}/'	, $infolettre , $body);

		
		$body = preg_replace('/{key9}/'	, 'Langue', $body);
		
		$body = preg_replace('/{val9}/'	, $_POST['langue'] , $body);


		$body = preg_replace('/{thedate}/'	, date('l F j Y, H:i:s'), $body);


		$mail->AddAddress(ADMIN_ROUGE, "julie@rougemarketing.com");
		$mail->AddAddress(ROUGE, "line@rougemarketing.com");
		$mail->AddAddress(CLIENT1, "fernandsabourin@royallepage.ca");
		$mail->AddAddress(CLIENT2, "cgrenon.sutton@gmail.com");
		//$mail->AddAddress(ROUGE, "isabelle@rougemarketing.com");


	break;
	
}

if($_POST["subject"] == "")

{
	
	$mail->SetFrom(CLIENT, 'Site web Station Nature');

	$mail->CharSet 		= 'UTF-8';

	$mail->Subject  	= 	$subjectmail;

	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

	$mail->MsgHTML($body);

}

					

		
		if( $mail->Send() ) {
			$form_action = 1;
			echo $form_action;
			/*echo '<script type="text/javascript">window.location = "' . $_SERVER['HTTP_REFERER'] . '?error"</script>';*/
			exit;
		}else{
			$form_action = 2;
			echo $form_action;
			/*echo '<script type="text/javascript">window.location = "' . $_SERVER['HTTP_REFERER'] . '?success"</script>';*/
			exit;
			
		}
		










/*****************************************************	

	MAILCHIMP DATA 

*****************************************************/		





/***********************************/	


function sendDataToMailChimp($listId, $courriel, $data){	


$apikey = "d90a55a5855dcfa32adb7dbc11e74b27-us14";

$endpoint   = "http://us14.api.mailchimp.com/3.0/lists/";      // find your datacenter in your apikey( xxxxxxxxxxxxxxxxxxxxxxxx-us13 <= this is your datacenter)
$auth       = base64_encode( 'user:'. $apikey );

		
			$json_data = json_encode($data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $endpoint.$listId.'/members/');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
														'Authorization: Basic '.$auth));
			curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
			
			$result = curl_exec($ch);
			
			if ( empty($result) ){
				$form_action = 3;
				/*echo $form_action;
				exit();*/
			}
			
			/*echo "<pre>";  // Response form mailchimp
			print_r(json_decode($result,true));*/
			
			
			$json = json_decode($result);
			//echo '<br />1-'.$json->{'status'};
			/*exit();*/
			
			
			
			if( $json->{'status'} == 400){  // DO update if user already in my list
				//DO UPDATE
				//echo '<br />2-'.$json->{'status'};
				//exit();
				$userid = md5($courriel);
					
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $endpoint.$listId.'/members/' . $userid);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
					'Authorization: Basic '. $auth));
				curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
				$result = curl_exec($ch);
				
				
			    /*echo "<pre>";  // Response form mailchimp
				print_r(json_decode($result,true));	*/
				
				if ( empty($result) ){
					$form_action = 3;
					/*echo $form_action;
					exit();*/
				}
			
			
			
				$json = json_decode($result,true);
				//echo $json->{'status'};
			
			
			}


}

/***************************/


/*****************************************************	

	END MAILCHIMP DATA 

*****************************************************/		
