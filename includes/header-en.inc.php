<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9JXKXG"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<script type="text/javascript">
  __AGDATA = {
    AdName: "stationnature.com",
    ConvType: "",
    CustomerID: 0,
    PageType: "",
    Language: "",
    Data: "",
    Revenue: 0
  };
</script>
<script type="text/javascript" src="//cdn.adgrx.com/usegments/cXhxpq3GSuZPc3UEBu4bhXxkZnNEW2j96xyWG6lN7Qk=/106.js" async="true"></script>

<div id="top"></div>
<section id="header">
    <div class="inner">
    
    		<div class="logo">
       	    	<a href="http://stationnature.com/en/"><img id="img-logo" src="../images/sation-nature.png" width="305" height="36" alt="Station Nature"/></a>
            </div><!-- logo -->
               
            
            <a href="#" id="menu-icon">&#9776;</a>
            
      
            <nav id="menu-principal">
            	<ul class="menu">
                	<li class="btn-space"><a href="tel:8193414663" class="btnVert"><img src="//rtb.adgrx.com/segments/RO_RjZ94cig87DT7pnnCynY5NwtdUJn9zGKDOxZFhaY=/30208.gif" width="1" height="1" border="0" />819 341-4663</a></li>
                	<li class="btn-space"><a href="information-request.php" class="btnBlanc">Information Request</a></li>
                	<li><a href="http://stationnature.com/en/#site">Site</a></li>
                	<li><a href="http://stationnature.com/en/#plans">Plan</a></li>
                	<li><a href="http://stationnature.com/en/#project">Project</a></li>
                	<li><a href="http://stationnature.com/en/#contact">Contact</a></li>
                	<li><a href="../">FR</a></li>
                </ul>
            </nav><!-- menu-principal -->
    
    </div><!-- inner-header -->
</section><!-- #header -->