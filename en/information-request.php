<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">
<title>Station Nature | Condominiums Mont-Tremblant | Information request</title>
<meta name="description" content="Station Nature Condominiums: contemporary living spaces, two to three bedrooms, best price-quality ratio, St-Jovite sector of Mont-Tremblant.">

<meta property="og:image" content="http://stationnature.com/images/D-8586_500x395-A.jpg" />

<script>
dataLayer = [];
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9JXKXG');</script>
<!-- End Google Tag Manager -->


<script src="../js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../js/jquery.sliderPro.min.js"></script>
<script type="text/javascript" src="../js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="../js/jquery.validationEngine-fr.js"></script>

<link rel="stylesheet" type="text/css" href="../css/slider-pro.min.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../js/fancybox/jquery.fancybox.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="../css/validationEngine.jquery.css" media="screen"/>


<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="../css/reset.css" rel="stylesheet" type="text/css" />
<link href="../css/style.min.css" rel="stylesheet" type="text/css" />

<script src="../js/scripts.js"></script>

</head>

<body>

<?php include('../includes/header-en.inc.php'); ?>


<section id="information" class="row1">
	<div class="inner">
    
    			<br />
                <div class="txt"><p>For more information about Station Nature,<br />
				please complete the form below.</p></div><br /><br />
        
        
        		<div id="msg"></div>
        
                <form id="form_info_en" name="form_info_en" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                
                <br />
                	
                    How did you hear about the project? 
                    <br />
                	<select id="entendu_parler" name="entendu_parler" class="validate[required]">
                        <option value="">Make your choice :</option>
                        <option value="Web site">Web site</option>
                        <option value="Social media">Social media</option>
                        <option value="Signage">Signage</option>
                        <option value="Highway signage">Highway signage</option>
                        <option value="Newspaper">Newspaper</option>
                        <option value="Magazine">Magazine</option>
                        <option value="Friend">Friend</option>
                        <option value="Other">Other, please specify:</option>
                    </select>
                    
                    <div class="precisez"><input type="text" id="autre_precisez" name="autre_precisez" placeholder="Specify" value=""><br /></div>
                    
                    
                	<input id="nom" name="nom" type="text" placeholder="First Name*" class="validate[required], minSize[2]]" /><br />
                	<input id="prenom" name="prenom" type="text" placeholder="Last Name*" class="validate[required], minSize[2]]" /><br />
                	<input id="courriel" name="courriel" type="text" placeholder="Email*" class="validate[required,custom[email], minSize[2]]" /><br />
                	<input id="telephone" name="telephone" type="text" placeholder="Phone" /><br />
                    <input id="code_postal" name="code_postal" type="text" placeholder="Postal code*" class="validate[required], minSize[2]]" /><br />
                	<textarea id="commentaires" name="commentaires" placeholder="Comments"></textarea>
                   <p style="font-size:11px; line-height:12px;"> *This field is required</p>
<br />
					<input id="infolettre" name="infolettre" type="checkbox" value="oui" checked="checked" /> I accept receiving information about Station Nature.


                	<input id="subject" name="subject" type="hidden" value="" /><br />
                	<input id="action" name="action" type="hidden" value="info" /><br />
                	<input id="langue" name="langue" type="hidden" value="en" />
                	<input type="submit" id="btnSubmit" name="btnSubmit" value="Send" class="btn" />
                
                </form><!-- form-info -->
    	
                
    </div><!-- inner -->
</section><!-- projet / row1 -->

<?php include('../includes/footer-en.inc.php'); ?>


</body>
</html>
